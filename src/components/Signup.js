import React, { Component } from 'react';
import styled, { keyframes } from 'styled-components';

const fadeIn = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

const Title = styled.p`
  text-align: center;
  margin-top: 80px;
`;

const SInput = styled.input`
  display: block;
  margin: 0 auto;
  background: inherit;
  border: 1px solid white;
  padding: 0.5em;
  width: 15;
  border-radius: 20px;
  transition: 500ms ease-out;
  &:focus {
    background: white;
  }
`;

const SButton = styled.button`
  display: block;
  margin: 0 auto;
  background: inherit;
  border: 1px solid white;
  padding: 0.5em;
  width: 15;
  border-radius: 20px;
  transition: 500ms ease-out;
  &:hover {
    background: white;
  }
`;

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      username: '',
      password: '',
    };
  }

  onPress = async () => {
    const res = await fetch('/signup', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        username: this.state.username,
        password: this.state.password,
      }),
    });

    const resjson = await res.json();

    if (resjson.success) {
      alert('Created successfully!');
      this.props.history.push('/login');
    } else {
      alert(resjson.value);
    }
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  render() {
    return (
      <div>
        <Title>Email</Title>
        <SInput name="email" onChange={e => this.onChange(e)} />
        <Title>Username</Title>
        <SInput name="username" onChange={e => this.onChange(e)} />
        <Title>Password</Title>
        <SInput
          name="password"
          onChange={e => this.onChange(e)}
          type="password"
        />
        <Title>
          <SButton name="Submit" type="submit" onClick={this.onPress}>
            Signup
          </SButton>
        </Title>
      </div>
    );
  }
}

export default Signup;
