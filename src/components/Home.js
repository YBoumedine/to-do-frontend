import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled, { keyframes } from 'styled-components';

const Title = styled.h1`
  text-align: center;
  padding: 1em;
`;

const fadeIn = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
`;

const SLink = styled(Link)`
  display: inline-block;
  width: 250px;
  height: 20px;
  border-radius: 20px;
  text-align: center;
  border: 1px solid white;
  padding: 0.5em;
  margin: 1em;
  color: white;
  text-decoration: none;
  transition: 0.5s ease-in-out;
  &:hover {
    animation: 1s ${fadeIn} ease-out;
    background-color: white;
    color: blue;
  }
`;

const ListItem = styled.li`
  text-align: center;
  list-style: none;
`;

class Home extends Component {
  render() {
    return (
      <div>
        <Title>To Do List</Title>
        <ListItem>
          <SLink to="/login"> Login </SLink>
        </ListItem>
        <ListItem>
          <SLink to="/signup"> Signup </SLink>
        </ListItem>
      </div>
    );
  }
}

export default Home;
