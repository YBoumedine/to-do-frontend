import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled, { keyframes } from 'styled-components';
import { FaHome, FaSignOutAlt, FaSign } from 'react-icons/fa';

const fadeIn = keyframes`
  0% {    
    opacity: 50%;
  }
  100% {
    opacity: 1;
  }
`;

const fadeOut = keyframes`
  0% {    
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
`;

const SLink = styled(Link)`
  display: inline;
  color: white;
  margin: 0.5em;
  text-decoration: none;
  position: absolute;
  left: ${props => (props.right ? 80 : 0)};
  top: 0;
  right: ${props => (props.right ? 0 : 80)};
`;

const SDiv = styled.div`
  margin-top: 200px;
  align-self: center;
`;

const Title = styled.p`
  text-align: center;
  margin-top: 50px;
`;

const SInput = styled.input`
  display: block;
  margin: 0 auto;
  background: inherit;
  border: 1px solid white;
  padding: 0.5em;
  width: 15;
  border-radius: 20px;
  outline: none;
  transition: 1s ease-out;
  &:focus {
    background: white;
    animation: 1s ${fadeIn} ease-in;
  }
`;

const SButton = styled.button`
  display: block;
  margin: 0 auto;
  background: inherit;
  border: 1px solid white;
  padding: 0.5em;
  width: 15;
  border-radius: 20px;
  transition: 1s ease-out;
  &:enabled {
    animation: 1s ${fadeIn} ease-in;
  }
  &:hover {
    background: white;
    animation: 1s ${fadeIn} ease-in;
  }
`;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  componentDidMount() {
    const userconn = localStorage.getItem('user-id');
    if (userconn) {
      this.props.history.push('/me?id=' + userconn);
    }
  }

  validate() {
    return this.state.username > 0 && this.state.password > 0;
  }

  handleClick = async e => {
    const res = await fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
      }),
    });

    const resjson = await res.json();

    if (resjson.success === false) {
      alert(resjson.message);
    } else {
      localStorage.setItem('user-id', resjson.message.id);
      localStorage.setItem('jwt-token', resjson.message.token);
      this.props.history.push('/me?id=' + resjson.message.id);
    }
  };

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  render() {
    return (
      <div>
        <span>
          <SLink to="/">
            <FaHome size={20} />
          </SLink>
        </span>
        <SDiv>
          <Title>Username</Title>
          <SInput
            type="text"
            name="username"
            value={this.state.username}
            onChange={e => this.handleChange(e)}
            required
          />
          <Title>Password</Title>
          <SInput
            type="password"
            name="password"
            value={this.state.password}
            onChange={e => this.handleChange(e)}
            required
          />
          <Title>
            <SButton
              onClick={this.handleClick}
              disabled={
                this.state.username.length > 0 && this.state.password.length > 0
                  ? false
                  : true
              }
            >
              Submit
            </SButton>
          </Title>
        </SDiv>
      </div>
    );
  }
}

export default Login;
