import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { stringify } from 'querystring';
import styled, { keyframes } from 'styled-components';
import { FaHome, FaSignOutAlt, FaCheck, FaTimes } from 'react-icons/fa';

const fadeIn = keyframes`
  0% {    
    opacity: 50%;
  }
  100% {
    opacity: 1;
  }
`;

const fadeOut = keyframes`
  0% {    
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
`;

const SLink = styled(Link)`
  display: inline;
  color: white;
  margin: 0.5em;
  text-decoration: none;
  position: absolute;
  left: ${props => (props.right ? 80 : 0)};
  top: 0;
  right: ${props => (props.right ? 0 : 80)};
`;

const STitle = styled.p`
  margin: auto;
`;

const SSpan = styled.span`
  position: absolute;
  flex: row;
  top: 0;
  left: 0;
  width: 100%;
  height: 5%;
  margin: 10px auto;
  &#createList {
    position: relative;
    display: flex;
    border: 1px solid white;
    border-radius: 20px;
    width: 90%;
  }
  &#item {
    width: 50%;
    display: flex;
    align-content: flex-end;
    border: 1px solid white;
    border-radius: 20px;
    position: relative;
  }
  &#list {
    width: 50%;
    display: flex;
    align-content: flex-end;
    border: 1px solid white;
    border-radius: 20px;
    position: relative;
  }
`;

const SDiv = styled.div`
  display: block;
  .container {
    align-self: auto;
    width: 99%;
    height: 80%;
    position: absolute;
    top: 10%;
    display: flex;
    flex-direction: row;
  }
  .listNames {
    display: flex;
    flex-direction: column;
    border: 3px solid rgba(0, 0, 39, 0.4);
    border-radius: 10px;
    width: 40%;
    height: 100%;
    overflow-y: scroll;
  }
  .listItems {
    display: flex;
    flex-direction: column;
    margin-left: 100px;
    border: 3px solid rgba(0, 0, 39, 0.4);
    width: 60%;
    border-radius: 10px;
  }
`;

const SInput = styled.input`
  display: block;
  margin: 0 auto;
  margin-bottom: 10px;
  background: inherit;
  padding: 0.5em;
  width: 15;
  height: 50%;
  outline: none;
  border: none;
`;

const SButton = styled.button`
  display: block;
  margin: 0 auto;
  background: inherit;
  border: 1px solid white;
  padding: 0.5em;
  border-radius: 20px;
  transition: 1s ease-out;
  &:enabled {
    animation: 1s ${fadeIn} ease-in;
  }
  &:hover {
    background: white;
    animation: 1s ${fadeIn} ease-in;
  }
  &.listButton {
    width: 80%;
    border: none;
    outline: none;
    &:hover {
      animation: none;
      background: inherit;
      cursor: pointer;
    }
  }
  &.createListButton {
    border: none;
  }
  &.removeItemButton {
    margin-top: 3px;
    border: none;
    position: absolute;
    right: 5%;
  }
  &.removeListButton {
    margin-top: 3px;
    border: none;
    position: absolute;
    right: 5%;
  }
`;

const DisconnectButton = styled(SButton)`
  display: inline;
  border: none;
  color: white;
  position: absolute;
  top: 0;
  right: 0;
  transition: none;
  outline: none;
  &:hover {
    background: inherit;
    animation: none;
  }
  &:enabled {
    animation: none;
  }
`;

class Me extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lists: [],
      newlist: '',
      newitem: '',
      entries: [],
      currentlist: '',
    };
  }

  fetchData = async () => {
    const _params = {
      id: localStorage.getItem('user-id'),
    };

    const url = '/me?' + stringify(_params);

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Authorization: `${localStorage.getItem('jwt-token')}`,
      },
    });

    const resjson = await res.json();
    this.setState({
      lists: resjson,
    });
  };

  componentDidMount() {
    const token = localStorage.getItem('jwt-token');
    const id = localStorage.getItem('user-id');
    // const params_id = new URLSearchParams(this.props.location.search).get('id');

    if (!token || !id) {
      localStorage.removeItem('jwt-token');
      localStorage.removeItem('user-id');
      this.props.history.push('/login');
    } else {
      this.fetchData();
    }
  }

  disconnectClick = () => {
    localStorage.removeItem('jwt-token');
    localStorage.removeItem('user-id');
    this.props.history.push('/');
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  loadEntries = async e => {
    const _params = {
      id: localStorage.getItem('user-id'),
      title: e.target.name,
    };
    const url = '/me/getList?' + stringify(_params);

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Authorization: `${localStorage.getItem('jwt-token')}`,
      },
    });

    const resjson = await res.json();

    this.setState({
      entries: resjson.entries,
      currentlist: _params.title,
    });
  };

  insertEntry = async e => {
    // e.preventDefault()
    const _body = {
      id: localStorage.getItem('user-id'),
      title: this.state.currentlist,
      entry: this.state.newitem,
    };

    const res = await fetch('/me/insertEntry', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `${localStorage.getItem('jwt-token')}`,
      },
      body: JSON.stringify(_body),
    });

    const resjson = await res.json();
    this.setState({
      entries: resjson.entries,
    });
  };

  removeEntry = async (e, itemkey) => {
    const _params = {
      id: localStorage.getItem('user-id'),
      title: this.state.currentlist,
      index: itemkey,
    };
    const url = '/me/removeEntry?' + stringify(_params);
    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Authorization: `${localStorage.getItem('jwt-token')}`,
      },
    });
    const resjson = await res.json();
    console.log(resjson);
    this.setState({
      entries: resjson.entries,
    });
  };

  createList = async () => {
    const _body = {
      id: localStorage.getItem('user-id'),
      title: this.state.newlist,
    };

    const res = await fetch('/me/createList', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `${localStorage.getItem('jwt-token')}`,
      },
      body: JSON.stringify(_body),
    });
    let resjson = await res.json();
    this.setState(prevState => ({
      lists: [...prevState.lists, resjson],
    }));
  };

  deleteList = async (e, _title) => {
    const _params = {
      id: localStorage.getItem('user-id'),
      title: _title,
    };

    const url = '/me/deleteList?' + stringify(_params);

    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Authorization: `${localStorage.getItem('jwt-token')}`,
      },
    });
    const resjson = await res.json();
    this.setState({
      lists: resjson,
    });
  };

  render() {
    let listTitles = this.state.lists.map(list => {
      return (
        <SSpan id="list" key={list.listid}>
          <SButton
            className="listButton"
            name={list.title}
            key={list.listid}
            onClick={e => this.loadEntries(e)}
          >
            {list.title}
          </SButton>
          <SButton
            className="removeListButton"
            onClick={e => this.deleteList(e, list.title)}
          >
            <FaTimes />
          </SButton>
        </SSpan>
      );
    });

    let listItems = null;

    if (this.state.currentlist.length > 0) {
      listItems = this.state.entries.map(entry => {
        return (
          <SSpan id="item" key={entry._id}>
            <STitle>{entry.entry}</STitle>
            <SButton
              className="removeItemButton"
              itemkey={entry._id}
              onClick={e => this.removeEntry(e, entry._id)}
            >
              <FaTimes />
            </SButton>
          </SSpan>
        );
      });
    }

    return (
      <SDiv>
        <SSpan>
          <SLink to="/">
            <FaHome size={20} />
          </SLink>
          <DisconnectButton onClick={this.disconnectClick}>
            <FaSignOutAlt size={20} />
          </DisconnectButton>
        </SSpan>
        <SDiv className="container">
          <SDiv className="listNames">
            <SSpan id="createList">
              <SInput
                placeholder="Create new list.."
                name="newlist"
                className="inputCreateList"
                type="text"
                value={this.state.newlist}
                onChange={e => this.handleChange(e)}
              />
              <SButton
                className="createListButton"
                disabled={this.state.newlist.length > 0 ? false : true}
                onClick={this.createList}
              >
                <FaCheck />
              </SButton>
            </SSpan>
            {listTitles}
          </SDiv>
          <SDiv className="listItems">
            <SSpan id="createList">
              <SInput
                placeholder="Create new item.."
                name="newitem"
                className="inputCreateList"
                type="text"
                value={this.state.newitem}
                onChange={e => this.handleChange(e)}
              />
              <SButton
                className="createListButton"
                disabled={this.state.newitem.length > 0 ? false : true}
                onClick={this.insertEntry}
              >
                <FaCheck />
              </SButton>
            </SSpan>
            {listItems}
          </SDiv>
        </SDiv>
      </SDiv>
    );
  }
}

export default Me;
