import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import Login from './components/Login';
import Me from './components/Me';
import Home from './components/Home';
import Signup from './components/Signup';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/login" exact component={Login} />
          <Route path="/me/:id?" component={Me} />
          <Route path="/signup" exact component={Signup} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
